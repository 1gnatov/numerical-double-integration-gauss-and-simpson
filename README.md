Решение двойных интегралов (по методу Гаусса и Симпсона).

Не до конца готовый продукт, но достаточный для интегрирования + построения графиков зависимости функции от параметра. Можно отрефакторить классы по построениям графиков.

Лицензия: APACHE 2.0

Numerical double integration with Gauss and Simpson methods, limits of polynomial power 37 with Gauss integration.
License: APACHE 2.0
