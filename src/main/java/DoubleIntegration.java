import java.util.List;

@SuppressWarnings("ALL")
public class DoubleIntegration {

  public static void main(String[] args) {
//    epsDependenceOfTau();
    dependenceOfYControlPoints();
  }

  public static PlotResult epsDependenceOfTau() {
    int xControlPoints = 10;
    int yControlPoints = 10;

    double x_a = 0;
    double x_b = Math.PI/2;
    double y_a = 0;
    double y_b = Math.PI/2;

    PlotResult result = new PlotResult();

    List<Double> t = LeganderPolynomRoots.findRoots(LeganderPolynomCalculator.calculate(xControlPoints));
    double[] A = LinearEquationGauss.solveGauss((new LeganderMatrix(t)).getMatrix(), LeganderMatrix.calculateValuesVector(xControlPoints));

    for (double tau = 0.05; tau<10; tau += 0.05) {
      double sum = calculation(xControlPoints, yControlPoints, x_a, x_b, y_a, y_b, A, tau);
      result.getX().add(tau);
      result.getY().add(sum);
      System.out.println(String.format("With Tau = %f, result = %f", tau, sum));
    }
    return result;
  }

  private static double calculation(int xControlPoints, int yControlPoints, double x_a, double x_b,
                                    double y_a, double y_b, double[] A, double tau) {
    double sum = 0;
    for (int i=0; i<xControlPoints; i++) {
      double x_i = (x_b-x_a)/xControlPoints*i;
      // tetta = y, phi = x;
      double finalTau = tau;
      double F_i = SimpsonIntegration.integrate(y_a, y_b, yControlPoints, (y) ->
          (1.-Math.exp(-1* finalTau *(2*Math.cos(y)/(1. - Math.sin(y)*Math.sin(y)*Math.cos(x_i)*Math.cos(x_i)))))*Math.cos(y)*Math.sin(y));
      sum += A[i]*F_i*(x_b-x_a)*2/Math.PI;
    }
    return sum;
  }

  public static PlotResult dependenceOfYControlPoints() {
    double tau = 0.5;
    int xControlPoints = 10;

    double x_a = 0;
    double x_b = Math.PI/2;
    double y_a = 0;
    double y_b = Math.PI/2;

    PlotResult result = new PlotResult();

    List<Double> t = LeganderPolynomRoots.findRoots(LeganderPolynomCalculator.calculate(xControlPoints));
    double[] A = LinearEquationGauss.solveGauss((new LeganderMatrix(t)).getMatrix(), LeganderMatrix.calculateValuesVector(xControlPoints));

    for (int yControlPoints = 4; yControlPoints<30; yControlPoints++) {
      double sum = calculation(xControlPoints, yControlPoints, x_a, x_b, y_a, y_b, A, tau);
      if (Double.isFinite(sum)) {
        result.getX().add((double) yControlPoints);
        result.getY().add(sum);
        System.out.println(String.format("With yControlPoints = %d, result = %f", yControlPoints, sum));
      }

    }
    return result;
  }

  public static PlotResult dependenceOfXControlPoints() {
    double tau = 0.5;
    int yControlPoints = 10;

    double x_a = 0;
    double x_b = Math.PI/2;
    double y_a = 0;
    double y_b = Math.PI/2;

    PlotResult result = new PlotResult();

    for (int xControlPoints = 4; xControlPoints<30; xControlPoints++) {
      List<Double> t = LeganderPolynomRoots.findRoots(LeganderPolynomCalculator.calculate(xControlPoints));
      double[] A = LinearEquationGauss.solveGauss((new LeganderMatrix(t)).getMatrix(), LeganderMatrix.calculateValuesVector(xControlPoints));
      double sum = calculation(xControlPoints, yControlPoints, x_a, x_b, y_a, y_b, A, tau);
      if (Double.isFinite(sum)) {
        result.getX().add((double) xControlPoints);
        result.getY().add(sum);
        System.out.println(String.format("With xControlPounts = %d, result = %f", xControlPoints, sum));
      }

    }
    return result;
  }

  public static PlotResult getDifference(PlotResult plotResult) {

    PlotResult difference = new PlotResult();
    for (int i=1; i<plotResult.getX().size(); i++) {
      difference.getX().add(plotResult.getX().get(i));
      difference.getY().add(Math.abs(plotResult.getY().get(i)-plotResult.getY().get(i-1)));
    }
    return difference;
  }

}
