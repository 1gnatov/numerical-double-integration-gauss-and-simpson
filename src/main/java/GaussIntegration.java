import java.util.List;

public class GaussIntegration {

  public static double integrate(double a, double b, int leganderPower, Function f) {

    List<Double> t = LeganderPolynomRoots.findRoots(LeganderPolynomCalculator.calculate(leganderPower));
    double[] A = LinearEquationGauss.solveGauss((new LeganderMatrix(t)).getMatrix(), LeganderMatrix.calculateValuesVector(leganderPower));

    double sum = 0;
    for (int i=0; i<leganderPower; i++) {
      sum += A[i]*f.apply((b+a)/2+(b-a)/2*t.get(i));
    }
    sum *= (b-a)/2;
    return sum;
  }

  public static double integrateWithPrecise(double a, double b, double eps, Function f) {
    double previousSum = eps + 1, sum = 0; //I-предыдущее вычисленное значение интеграла, I1-новое, с большим N.
    int n=3;
    for (; Math.abs(sum - previousSum) > eps; n += 1) {
      previousSum = sum;
      sum = integrate(a, b, n, f);
    }
    //System.out.println(I1);
    System.out.println("Gauss result legander power = " + n);
    return sum;
  }



}
