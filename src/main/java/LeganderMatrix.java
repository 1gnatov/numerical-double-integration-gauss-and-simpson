import java.util.Arrays;
import java.util.List;

public class LeganderMatrix {
  private double[][] matrix;

  public LeganderMatrix(List<Double> leganderRoots) {
    int size = leganderRoots.size();
    double[][] matrix = new double[size][size];
    for (int i = 0; i<size; i++) {
      for (int j = 0; j<size; j++) {
        matrix[i][j] += Math.pow(leganderRoots.get(j), i);
      }
    }
    this.matrix = matrix;
  }

  public double[][] getMatrix() {
    return matrix;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (double[] doubles : matrix) {
      sb.append(Arrays.toString(doubles)).append("\n");
    }
    return sb.toString();
  }

  public static double[] calculateValuesVector(int m) {
    double[] result = new double[m];
    for (int i=0; i<m; i++) {
      if (i%2==0) {
        result[i] = 2./(i+1);
      } else {
        result[i] = 0;
      }
    }
    return result;
  }
}
