
public class LeganderPolynomCalculator {

  public static Polynom calculate(int m) {
    Polynom pMminus2 = (new Polynom()).setValue(0, 1);
    Polynom pMminus1 = (new Polynom()).setValue(1, 1);
    if (m == 0) {
      return pMminus2;
    }
    if (m==1) {
      return pMminus1;
    }
    int currentM = 2;
    Polynom result = null;
    while (currentM <= m) {
      result = getNextPolynom(currentM, pMminus2, pMminus1);
      pMminus2 = pMminus1;
      pMminus1 = result;
      currentM += 1;
    }
    return result;

  }

  private static Polynom getNextPolynom(int m, Polynom pMminus2, Polynom pMminus1) {
    return pMminus1.multiply(2 * m - 1).power(1).minus((pMminus2.multiply(m - 1))).divide(m);
  }
}
