import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class LeganderPolynomRoots {

  public static List<Double> findRoots(Polynom polynom) {
    int arraySize = 100000;
    double step = 1./arraySize;
    List<Double> xNearRoots = new ArrayList<>();
    List<Double> xRoots;
    List<Double> resultRoots;
    double eps = 0.00000000000001;

    if (polynom.apply(0.) == 0.) {
      xNearRoots.add(0.);
    }

    double x_i_last = 0;
    double x_i = 0;
    double y_i_last = 0;
    double y_i = 0;

    for (int i=0; i<arraySize; i++) {
      if (i > 0) {
        x_i_last = x_i;
        y_i_last = y_i;
      }
      x_i = i*step;
      y_i = polynom.apply(x_i);
      if (i>0) {
        if (y_i_last*y_i < 0) {
          xNearRoots.add(x_i_last);
        }
      }
    }
    xRoots = binomialFindRoot(polynom, xNearRoots, step, eps);
    Set<Double> resultRootsSet = new HashSet<>(xRoots);
    resultRootsSet.addAll(xRoots.stream().map(e -> e*-1).filter(e -> e != -0.0).collect(Collectors.toSet()));
    resultRoots = resultRootsSet.stream().sorted().collect(Collectors.toList());
    return resultRoots;
  }

  private static List<Double> binomialFindRoot(Polynom polynom, List<Double> xNearRoots,
                                               double step, double epsilon) {
    List<Double> roots = new ArrayList<>();
    for (Double nearRoot : xNearRoots) {
      Double left = nearRoot;
      double right = left + step;
      while (Math.abs(polynom.apply(left)) > epsilon) {
        double middle = (left+right)/2.;
        if (polynom.apply(left)*polynom.apply(middle) < 0) {
          right = middle;
        } else {
          if (middle - left < epsilon) {
            double middleValue = polynom.apply(middle);
            double rightValue = polynom.apply(right);
            if (Math.abs(middleValue) < Math.abs(rightValue)) {
              left = middle;
            } else {
              left = right;
            }
            break;
          }
          left = middle;
        }
      }
      roots.add(left);
    }
    return roots;
  }
}
