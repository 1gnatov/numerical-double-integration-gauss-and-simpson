public class LinearEquationGauss {

  public static double[] solveGauss(double[][] z, double[] y)
  {
    int n = y.length;
    double[] x = new double[n];
    double[][] a = new double[n][n];
    for (int i=0; i<n; i++) {
      System.arraycopy(z[i], 0, a[i], 0, n);
    }
    double[] b = new double[n];
    System.arraycopy(y, 0, b, 0, n);

    //printMatrixAndVector("GaussM before, a, b, n", a, b);

    for (int k = 0; k < n; k++)
    {
      double d = a[k][k];
      for (int j = k; j < n; j++)
      {
        a[k][j] /= d;
      }
      b[k] /= d;
      for (int i = k + 1; i < n; i++)
      {
        double r = a[i][k]; // раньше делили на d
        for (int j = k; j < n; j++)
        {
          a[i][j] -= a[k][j] * r;
        }
        b[i] -= b[k] * r;
      }
    }
    //printMatrixAndVector("GaussM a, b, n", a, b);

    // находим последний корень
    x[n - 1] = b[n - 1];
    // идем снизу вверх
    for (int i = n - 2; i >= 0; i--)
    {
      double s = 0.0;
      for (int j = i + 1; j < n; j++)
      {
        s += a[i][j] * x[j];
      }

      x[i] = b[i] - s;
    }
    return x;
  }

  public static void printVector(String caption, double[] x)
  {
    System.out.println(caption);
    for (double d : x)
    {
      System.out.println(String.format("%.2f, ", d));
    }
  }

  public static void printMatrixAndVector(String text, double[][] a, double[] b) {
    int n = b.length;
    System.out.println(text);
    for (int i = 0; i < n; i++)
    {
      for (int j = 0; j < n; j++)
      {
        System.out.print(String.format("%f, ", a[i][j]));
      }
      System.out.println(String.format("%f, ", b[i]));
    }
  }
}



