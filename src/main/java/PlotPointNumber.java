import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class PlotPointNumber extends JFrame {

  public static final String TITLE =
      "Differences between integrations from numbers of control points (other variable number of points = 10)";

  public PlotPointNumber() {

    initUI();
  }

  public static void main(String[] args) {

    EventQueue.invokeLater(() -> {

      PlotPointNumber ex = new PlotPointNumber();
      ex.setVisible(true);
    });
  }



  private void initUI() {

    XYDataset dataset = createDataset();
    JFreeChart chart = createChart(dataset);

    ChartPanel chartPanel = new ChartPanel(chart);
    chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
    chartPanel.setBackground(Color.white);
    add(chartPanel);

    pack();
    setTitle("Line chart");
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private XYDataset createDataset() {

    PlotResult result = DoubleIntegration.getDifference(DoubleIntegration.dependenceOfYControlPoints());

    XYSeries series = new XYSeries("Inner integral (Simpson)");
    for (int i = 0; i<result.getX().size(); i++) {
      series.add(result.getX().get(i), result.getY().get(i));
    }
    PlotResult result2 = DoubleIntegration.getDifference(DoubleIntegration.dependenceOfXControlPoints());

    XYSeries series2 = new XYSeries("Outer integral (Gauss)");
    for (int i = 0; i<result2.getX().size(); i++) {
      series2.add(result2.getX().get(i), result2.getY().get(i));
    }

    XYSeriesCollection dataset = new XYSeriesCollection();
    dataset.addSeries(series);
    dataset.addSeries(series2);
    return dataset;
  }

  private JFreeChart createChart(XYDataset dataset) {

    JFreeChart chart = ChartFactory.createXYLineChart(
        TITLE,
        "control points",
        "Delta of integrations",
        dataset,
        PlotOrientation.VERTICAL,
        true,
        true,
        false
    );

    XYPlot plot = chart.getXYPlot();

    XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
    renderer.setSeriesPaint(0, Color.RED);
    renderer.setSeriesStroke(0, new BasicStroke(2.0f));

    plot.setRenderer(renderer);
    plot.setBackgroundPaint(Color.white);

    plot.setRangeGridlinesVisible(true);
    plot.setRangeGridlinePaint(Color.BLACK);

    plot.setDomainGridlinesVisible(true);
    plot.setDomainGridlinePaint(Color.BLACK);

    chart.getLegend().setFrame(BlockBorder.NONE);

    chart.setTitle(new TextTitle(TITLE,
            new Font("Serif", Font.BOLD, 18)
        )
    );

    return chart;
  }
}
