import java.util.ArrayList;
import java.util.List;

public class PlotResult {

  private List<Double> x = new ArrayList<>();
  private List<Double> y = new ArrayList<>();

  public List<Double> getY() {
    return y;
  }

  public List<Double> getX() {
    return x;
  }
}
