import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Polynom {

  public double[] array;

  public Polynom() {
    this.array = new double[200];
  };

  public Polynom(double[] array) {
    this.array = array;
  }

  public Polynom(Polynom polynom) {
    this.array = new double[polynom.array.length];
    System.arraycopy(polynom.array, 0, array, 0, polynom.array.length);
  }

  public int length() {
    return this.array.length;
  }

  public Polynom setValue(int index, double value) {
    this.array[index] = value;
    return this;
  }

  public static List<String> reverse(List<String> strings) {
    List<String> result = new ArrayList<>();
    for (int i=0; i<strings.size(); i++){
      result.add(strings.get(strings.size()-1-i));
    }
    return result;
  }

  public Polynom plus(Polynom p) {
    Polynom result = new Polynom();
    for (int i=0; i < this.array.length; i++) {
      result.array[i] += this.array[i];
      result.array[i] += p.array[i];
    }
    return result;
  }

  public Polynom minus(Polynom p) {
    Polynom result = new Polynom();
    for (int i=0; i < this.array.length; i++) {
      result.array[i] += this.array[i];
      result.array[i] -= p.array[i];
    }
    return result;
  }

  public Polynom multiply(double number) {
    Polynom result = new Polynom();
    for (int i=0; i < this.array.length; i++) {
      result.array[i] += this.array[i]*number;
    }
    return result;
  }

  public int getMaxPower() {
    int power = 0;
    for (int i=0; i<this.array.length; i++) {
      if (array[i] != 0.) {
        power = i;
      }
    }
    return power;
  }

  public Polynom divide(double number) {
    return this.multiply(1/number);
  }

  public Polynom power(int power) {
    Polynom result = new Polynom();
    for (int i=0; i < this.array.length-power; i++) {
      result.array[i+power] += this.array[i];
    }
    for (int i=this.array.length-power; i<this.array.length; i++) {
      if (this.array[i] != 0.0) {
        throw new IllegalArgumentException("Out of bound while multiplying X: " + i + " power " + power + " maxPower" + this.array.length);
      }
    }
    return result;
  }

  public Polynom multiply(Polynom polynom) {
    Polynom result = new Polynom(this);
    if (polynom.array[0] != 0.) {
      result = result.multiply(polynom.array[0]);
    }
    for (int i=1; i<polynom.array.length; i++) {
      if (polynom.array[i] != 0.) {
        Polynom temp = new Polynom(this);
        temp = temp.power(i);
        temp = temp.multiply(polynom.array[i]);
        result = result.plus(temp);
      }
    }
    return result;
  }

  public double apply(double x) {
    double result = 0.;
    for (int i=0; i<=this.getMaxPower(); i++) {
      if (array[i] != 0.) {
        result += Math.pow(x, i)*array[i];
      }
    }
    return result;
  }

  @Override
  public String toString() {
    List<String> strings = new ArrayList<>();
    for (int i=0; i<array.length; i++){
      if (array[i] != 0.)
        strings.add(String.format("%fx^%d", array[i], i));
    };
    return reverse(strings).stream().collect(Collectors.joining(" + "));
  }
}
