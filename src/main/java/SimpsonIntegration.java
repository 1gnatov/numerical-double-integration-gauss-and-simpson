public class SimpsonIntegration {

  public static double integrate(double a, double b, int n, Function f) {
    double h, sum2 = 0, sum4 = 0, totalSum = 0;
    h = (b - a) / (2 * n);
    for (int i = 1; i <= 2 * n - 1; i += 2) {
      sum4 += f.apply(a + h * i);
      sum2 += f.apply(a + h * (i + 1));
    }
    totalSum = f.apply(a) + 4 * sum4 + 2 * sum2 - f.apply(b); //Отнимаем значение f(b) так как ранее прибавили его дважды.
    double result = (h / 3) * totalSum;
    //System.out.println(result);
    return result;
  }

  public static double integrateWithPrecise(double a, double b, double eps, Function f) {
    double I = eps + 1, I1 = 0; //I-предыдущее вычисленное значение интеграла, I1-новое, с большим N.

    for (int N = 2; Math.abs(I1 - I) > eps; N *= 2) {
      I = I1;
      I1 = integrate(a, b, N, f);
    }
    //System.out.println(I1);
    return I1;
  }
}
