import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GaussIntegrationTest {

  public void gaussIntegrate(double a, double b, double eps, int leganderPower, Function func, double result) {
    double integrationResult = GaussIntegration.integrate(a, b, leganderPower, func);
    Assertions.assertEquals(result, integrationResult, eps);
    System.out.println(String.format("Difference = %f, eps = %f", result - integrationResult, eps));
  }

  public void gaussIntegrateWithPrecies(double a, double b, double eps, Function func, double result) {
    double integrationResult = GaussIntegration.integrateWithPrecise(a, b, eps, func);
    Assertions.assertEquals(result, integrationResult, eps);
    System.out.println(String.format("Difference = %f, eps = %f", result - integrationResult, eps));
  }

  @Test
  public void integrate() {
    gaussIntegrate(0.6, 2.2, 0.001,10, (x) -> Math.cos(x) / (x * x + 1), 0.198816); //https://www.wolframalpha.com/input/?i=integral+cos%28x%29%2F%28x%5E2%2B1%29+from+0.6+to+2.2
  }

  @Test
  public void integrate2() {
    gaussIntegrate(1, 3, 0.0001,10, (x) -> Math.sin(x * x + 2 * x), -0.143059); //https://www.wolframalpha.com/input/?i=integral+sin%28x%5E2%2B2*x%29+from+1+to+3
  }

  @Test
  public void integrate5() {
    gaussIntegrate(1, 3, 0.0001,10, (x) -> Math.cos(x * x + 2 * x), 0.0233549); //https://www.wolframalpha.com/input/?i=integral+sin%28x%5E2%2B2*x%29+from+1+to+3
  }

  @Test
  public void integrate3() {
    gaussIntegrate(1, 3, 0.0001,10, Math::sin, 1.530294802468); //https://www.wolframalpha.com/input/?i=integral+sin%28x%5E2%2B2*x%29+from+1+to+3
  }

  @Test
  public void integrate3WithPrecise() {
    gaussIntegrateWithPrecies(1, 3, 0.00001, Math::sin, 1.530294802468); //https://www.wolframalpha.com/input/?i=integral+sin%28x%5E2%2B2*x%29+from+1+to+3
  }

  @Test
  public void integrate4() {
    gaussIntegrate(1, 3, 0.0001,10, Math::cos, -0.700350976); //https://www.wolframalpha.com/input/?i=integral+sin%28x%5E2%2B2*x%29+from+1+to+3
  }

}
