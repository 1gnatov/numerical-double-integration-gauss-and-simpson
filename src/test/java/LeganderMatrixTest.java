import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class LeganderMatrixTest {

  @Test
  void m3() {
    Polynom polynom = LeganderPolynomCalculator.calculate(3);
    List<Double> roots = LeganderPolynomRoots.findRoots(polynom);
    LeganderMatrix leganderMatrix = new LeganderMatrix(roots);
    System.out.println(leganderMatrix);

    double[][] expected = new double[][] {{1., 1., 1.},
                                          {-1*Math.sqrt(3./5.), 0, Math.sqrt(3./5.)},
                                          {(3./5.), 0, (3./5.)}};

    Assertions.assertTrue(array2dEqual(expected, leganderMatrix.getMatrix()));
  }

  public boolean array2dEqual(double[][]expected, double[][]result) {
    double eps = 0.00000001;
    for (int i=0; i<expected.length; i++) {
      for (int j=0; j<expected.length; j++) {
        if (Math.abs(expected[i][j] - result[i][j]) > eps) {
          return false;
        }
      }
    }
    return true;
  }
}
