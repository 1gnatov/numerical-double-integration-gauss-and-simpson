import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeganderPolynomCalculatorTest {

  //https://ru.wikipedia.org/wiki/%D0%9C%D0%BD%D0%BE%D0%B3%D0%BE%D1%87%D0%BB%D0%B5%D0%BD%D1%8B_%D0%9B%D0%B5%D0%B6%D0%B0%D0%BD%D0%B4%D1%80%D0%B0

  public static boolean equalDoubleArray(double[] a1, double[] a2, int elemsToCheck){
    for (int i=0; i<elemsToCheck; i++) {
      if (a1[i] != a2[i]) return false;
    }
    return true;
  }

  @Test
  public void p0() {
    Polynom res = LeganderPolynomCalculator.calculate(0);
    Assertions.assertTrue(equalDoubleArray(new double[]{1., 0., 0., 0., 0., 0.}, res.array, 5));
  }

  @Test
  public void p1() {
    Polynom res = LeganderPolynomCalculator.calculate(1);
    Assertions.assertTrue(equalDoubleArray(new double[]{0., 1., 0., 0., 0., 0.}, res.array, 5));
  }

  @Test
  public void p2() {
    Polynom res = LeganderPolynomCalculator.calculate(2);
    Assertions.assertTrue(equalDoubleArray(new double[]{-0.5, 0., 1.5, 0., 0., 0.}, res.array, 5));
  }

  @Test
  public void p3() {
    Polynom res = LeganderPolynomCalculator.calculate(3);
    Assertions.assertTrue(equalDoubleArray(new double[]{0, -1.5, 0, 2.5, 0., 0.}, res.array, 5));
  }

  @Test
  public void p4() {
    Polynom res = LeganderPolynomCalculator.calculate(4);
    Assertions.assertTrue(equalDoubleArray(new double[]{3./8, 0, -30./8, 0, 35./8., 0., 0., 0., 0., 0.}, res.array, 10));
  }

  @Test
  public void p5() {
    Polynom res = LeganderPolynomCalculator.calculate(5);
    Assertions.assertTrue(equalDoubleArray(new double[]{0., 15./8, 0., -70./8, 0., 63./8, 0., 0., 0., 0.}, res.array, 10));
  }

  @Test
  public void p199() {
    Polynom res = LeganderPolynomCalculator.calculate(199);
  }
}
