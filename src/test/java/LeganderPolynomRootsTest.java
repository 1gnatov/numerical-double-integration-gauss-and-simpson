import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeganderPolynomRootsTest {

//  private String listToString(List<Double> list) {
//    return list.stream().sorted().map(Object::toString).collect(Collectors.joining(", "));
//  }

  @Test
  public void p1Roots() {
    Polynom polynom = LeganderPolynomCalculator.calculate(1);
    List<Double> roots = LeganderPolynomRoots.findRoots(polynom);
    Assertions.assertTrue(equalDoubleList(List.of(0.0), roots));
    for (Double root : roots) {
      Assertions.assertEquals(0.0, polynom.apply(root), 0.001);
    }
  }

  @Test
  public void p2Roots() {
    Polynom polynom = LeganderPolynomCalculator.calculate(2);
    List<Double> roots = LeganderPolynomRoots.findRoots(polynom);
    System.out.println(roots);
    for (Double root : roots) {
      Assertions.assertEquals(0.0, polynom.apply(root), 0.0001);
    }
  }

  @Test
  public void p3_30_Roots() {
    for (int m = 3; m < 31; m++) {
      Polynom polynom = LeganderPolynomCalculator.calculate(m);
      List<Double> roots = LeganderPolynomRoots.findRoots(polynom);
      Assertions.assertEquals(m, roots.size());
      for (Double root : roots) {
        Assertions.assertEquals(0.0, polynom.apply(root), 0.0001);
      }
    }
  }

  @Test
  public void p31_37_Roots() {
    for (int m = 31; m < 38; m++) {
      Polynom polynom = LeganderPolynomCalculator.calculate(m);
      List<Double> roots = LeganderPolynomRoots.findRoots(polynom);
      Assertions.assertEquals(m, roots.size());
      for (Double root : roots) {
        Assertions.assertEquals(0.0, polynom.apply(root), 0.1);
      }
    }
  }

  @Test
  public void p3Roots() {
    Polynom polynom = LeganderPolynomCalculator.calculate(2);
    List<Double> roots = LeganderPolynomRoots.findRoots(polynom);
    for (Double root : roots) {
      Assertions.assertEquals(0.0, polynom.apply(root), 0.0001);
    }
  }

  private boolean equalDoubleList(List<Double> expected, List<Double> roots) {
    double eps = 0.000001;
    for (int i = 0; i < expected.size(); i++) {
      if (Math.abs(expected.get(i) - roots.get(i)) > eps) {
        return false;
      }
      if (expected.size() != roots.size()) {
        return false;
      }
    }
    return true;
  }
}
