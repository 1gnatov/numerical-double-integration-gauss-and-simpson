import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class LinearEquationGaussTest {

  public static boolean checkMatrixEquations(double[][] matrix, double[] values, double[] roots) {
    double eps = 0.00001;
    int n = roots.length;
    for (int i=0; i<n; i++) {
      double sum = 0;
      for (int j=0; j<n; j++) {
        sum += matrix[i][j]*roots[j];
      }
      if (Math.abs(sum - values[i]) > eps) {
        return false;
      }
    }
    return true;
  }

  public static boolean arraysEqual(double[] expected, double[] actual) {
    double eps = 0.00001;
    for (int i=0; i<expected.length; i++) {
      if (Math.abs(expected[i] - actual[i]) > eps) {
        return false;
      }
    }
    return true;
  }

  @Test
  void legander3() {
    double[][] a =
        {
            {1, 1, 1},
            {-1*Math.sqrt(3./5.), 0, Math.sqrt(3./5.)},
            {3./5, 0, 3./5},
        };
    double[] b = {2, 0, 2./3.};

    LinearEquationGauss.printMatrixAndVector("Matrix a, vector b", a, b);
    double[] x = LinearEquationGauss.solveGauss(a, b);
    LinearEquationGauss.printVector("Vector x", x);

    double[] expected = {5./9, 8./9, 5./9};
    Assertions.assertTrue(arraysEqual(expected, x));
    Assertions.assertTrue(checkMatrixEquations(a, b, x));
  }

  @Test
  void legander3Integrotest() {
    int m = 3;
    LeganderMatrix matrix = new LeganderMatrix(LeganderPolynomRoots.findRoots(LeganderPolynomCalculator.calculate(m)));
    double[][] a = matrix.getMatrix();
    double[] b = LeganderMatrix.calculateValuesVector(m);

    LinearEquationGauss.printMatrixAndVector("Matrix a, vector b", a, b);
    double[] x = LinearEquationGauss.solveGauss(a, b);
    LinearEquationGauss.printVector("Vector x", x);

    double[] expected = {5./9, 8./9, 5./9};
    Assertions.assertTrue(arraysEqual(expected, x));
    Assertions.assertTrue(checkMatrixEquations(a, b, x));
  }

  @Test
  void legander2Integrotest() {
    int m = 2;
    LeganderMatrix matrix = new LeganderMatrix(LeganderPolynomRoots.findRoots(LeganderPolynomCalculator.calculate(m)));
    double[][] a = matrix.getMatrix();
    double[] b = LeganderMatrix.calculateValuesVector(m);

    LinearEquationGauss.printMatrixAndVector("Matrix a, vector b", a, b);
    double[] x = LinearEquationGauss.solveGauss(a, b);
    LinearEquationGauss.printVector("Vector x", x);

//    double[] expected = {5./9, 8./9, 5./9};
//    Assertions.assertTrue(arraysEqual(expected, x));
//    Assertions.assertTrue(checkMatrixEquations(a, b, x));
  }


  @Test
  void deon() {
    double[][] a =
        {
            {2.0, 4.0, 2.0, 4.0},
            {1.0, 1.0, 6.0, 3.0},
            {-1.0, -4.0, 1.0, 1.0},
            {4.0, 5.0, 9.0, 12.0}
        };
    double[] b = {22.0, 19.0, -10.0, 49.0};

    LinearEquationGauss.printMatrixAndVector("Matrix a, vector b", a, b);
    double[] x = LinearEquationGauss.solveGauss(a, b);
    LinearEquationGauss.printVector("Vector x", x);
    Assertions.assertTrue(checkMatrixEquations(a, b, x));
  }
}
