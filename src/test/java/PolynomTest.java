import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PolynomTest {

  public static boolean equalDoubleArray(double[] a1, double[] a2, int elemsToCheck){
    for (int i=0; i<elemsToCheck; i++) {
      if (a1[i] != a2[i]) return false;
    }
    return true;
  }

  @Test
  public void apply() {
    Polynom a = new Polynom(new double[]{1., 2., 1.5, 0., 0., 0., 0., 0., 0., 0.});
    double x = 2.1;
    Assertions.assertEquals(1.5*x*x + 2*x + 1, a.apply(x));
  }

  @Test
  public void maxPower() {
    Polynom a = new Polynom(new double[]{1., 2., 3., 0., 0., 0., 0., 0., 0., 0.});
    Assertions.assertEquals(2, a.getMaxPower());
  }

  @Test
  public void testToString() {
    double[] array = {1., 2., 3., 0., 0., 0., 0., 0., 22., 0.};
    Polynom p = new Polynom(array);
    System.out.println(p.toString());
  }

  @Test
  public void plus() {
    Polynom a = new Polynom(new double[]{1., 2., 3., 0., 0., 0., 0., 0., 0., 0.});
    Polynom b = new Polynom(new double[]{0., 0., 1., 4., 5., 0., 0., 0., 0., 0.});

    Polynom sum = a.plus(b);
    System.out.println(sum);
    Assertions.assertTrue(equalDoubleArray(new double[]{1., 2., 4., 4., 5., 0., 0., 0., 0., 0.}, sum.array, 5));
  }

  @Test
  void minus() {
    Polynom a = new Polynom(new double[]{1., 2., 3., 0., 0., 0., 0., 0., 0., 0.});
    Polynom b = new Polynom(new double[]{0., 0., 1., 4., 5., 0., 0., 0., 0., 0.});

    Polynom res = a.minus(b);
    System.out.println(res);
    Assertions.assertTrue(equalDoubleArray(new double[]{1., 2., 2., -4., -5., 0., 0., 0., 0., 0.}, res.array, 5));
  }

  @Test
  void multiplyNumber() {
    Polynom a = new Polynom(new double[]{1., 2., 3., 0., 0., 0., 0., 0., 0., 0.});

    Polynom res = a.multiply(2.);
    System.out.println(res);
    Assertions.assertTrue(equalDoubleArray(new double[]{2., 4., 6., 0., 0., 0., 0., 0., 0., 0.}, res.array, 3));
  }

  @Test
  void powerOnce() {
    Polynom a = new Polynom(new double[]{1., 2., 3., 0., 0., 0., 0., 0., 0., 0.});

    Polynom res = a.power(1);
    System.out.println(res);
    Assertions.assertTrue(equalDoubleArray(new double[]{0, 1., 2., 3., 0., 0., 0., 0., 0., 0.}, res.array, 4));
  }

  @Test
  void power() {
    Polynom a = new Polynom(new double[]{1., 2., 3., 0., 0., 0., 0., 0., 0., 0.});

    Polynom res = a.power(2);
    System.out.println(res);
    Assertions.assertTrue(equalDoubleArray(new double[]{0, 0, 1., 2., 3., 0., 0., 0., 0., 0.}, res.array, 5));
  }

  @Test
  void powerOverflow() {
    Polynom a = new Polynom(new double[]{1., 2., 3., 0., 0., 0., 0., 0., 0., 1.});
    Polynom res = a.divide(2);
    Assertions.assertTrue(equalDoubleArray(new double[]{0.5, 1, 1.5, 0., 0., 0., 0., 0., 0., 0.}, res.array, 5));
  }

  @Test
  void multipyPolinom() {
    Polynom a = new Polynom(new double[]{6., 4., 0., 0., 0., 0., 0., 0., 0., 0.});
    Polynom b = new Polynom(new double[]{2., 1., 0., 0., 0., 0., 0., 0., 0., 0.});

    Polynom res = a.multiply(b);
    Assertions.assertTrue(equalDoubleArray(new double[]{12, 14, 4, 0., 0., 0., 0., 0., 0., 0.}, res.array, 4));
  }
}
