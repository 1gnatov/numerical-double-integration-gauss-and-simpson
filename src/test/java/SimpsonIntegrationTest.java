import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SimpsonIntegrationTest {

  public void simpsonIntegrate(double a, double b, double eps, Function func, double result) {
    double integrationResult = SimpsonIntegration.integrateWithPrecise(a, b, eps, func);
    Assertions.assertEquals(result, integrationResult, eps);
    System.out.println(String.format("Difference = %f, eps = %f", result - integrationResult, eps));
  }

  @Test
  public void integrate() {
    simpsonIntegrate(0.6, 2.2, 0.001, (x) -> Math.cos(x) / (x * x + 1), 0.198816); //https://www.wolframalpha.com/input/?i=integral+cos%28x%29%2F%28x%5E2%2B1%29+from+0.6+to+2.2
  }

  @Test
  public void integrate2() {
    simpsonIntegrate(1, 3, 0.0001, (x) -> Math.sin(x * x + 2 * x), -0.143059); //https://www.wolframalpha.com/input/?i=integral+sin%28x%5E2%2B2*x%29+from+1+to+3
  }

  @Test
  public void integrate3() {
    simpsonIntegrate(1, 3, 0.0001, (x) -> Math.cos(x * x + 2 * x), 0.0233549); //https://www.wolframalpha.com/input/?i=integral+sin%28x%5E2%2B2*x%29+from+1+to+3
  }
}
